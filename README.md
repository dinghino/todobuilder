# TodoBuilder

Overkill solution to handle todo objects in typescript.

This project is meant to wrap incoming todos and tasks from a back end, parse them and allow
easy interaction in the client, or to build objects that can be sent safely to the server.

Includes data validation and conversion (dates into `moment.js` objects) and entity builder.

## Why?
Lots of stuff for todos... why not?
This project is an extension of another one where I needed to parse data coming from the server and I didn't want to do it on the fetch methods. This allows me to build entities quickly and with no pain and ensure that they have what I need how I need it.

## Quickstart

For now this is a standalone repo, so to use it you need to

    git clone https://dinghino@bitbucket.org/dinghino/todobuilder.git

and then you can either get the stuff in `./src/` and copy in some folder of your _typescript_ project or

    yarn build

that will output in `./dist` and do the same with that folder, to use it as standard javascript module.

## Testing

Tests are done with `alsatian`, coverage is done with `nyc` and `tap-diff`:

    yarn test     # runs the tests
    yarn cover    # runs tests and coverage. outputs HTML in ./coverage


## Usage

```typescript
import Builder, { Itodo, ITask } from './src' // what is this import? :v

// explicitly build a new Todo item on the client
const builder = new Builder().title('Do something').completed(true)
const todo = builder.buildTodo()
saveToStorage(todo.toJSON())

// Parse incoming data
const itemSource: Array<ITodo & ITask> = [ /* a generic array of JSON object from the back end */ ]

// build your items
itemSource.forEach(source => {
    // fromJSON() can be called either as static method or instance method
    const item = Builder.fromJSON(source).build()
    // discriminate what you created
    if (item.isTask())
      console.log(`TASK ITEM: ${item.title} due to ${item.dueTo.fromNow()}`)
    else
      console.log(`TODO ITEM: ${item.title}`)
})
```

The builder also resets itself after creating (or fail to do so) a new item,
so that you don't need to create a new instance on every item you create.

### Customization
You can customize the required fields for todo and task items in some ways:

* passing an `options` object upon Builder creation with the following shape (**favored method**)
* passing an _optional_ `options` object to the _static_ `fromJSON(data, options)`
* change the defaults by calling `Builder.setRequiredFields({...})`
  This will set the new required fields for all instances created after the change.
* by calling the proper method on the Builder **instance**.
  * `setRequiredTodoFields(string[])` sets the required fields for todos
  * `setRequiredTaskFields(string[])` sets the required fields for tasks

> Note that these are used for discriminating upon creation, so be sure that there is at least one difference.

#### Options object
```typescript
{
  todo?: string[],  // valid ITodo keys
  task?: string[],  // valid ITask keys
  strict?: boolean, // If true the default Builder values will not be used
}
```

Note that by default the Builder has no required fields, so unless you set them there is no
need to pass strict.

```typescript
import { Builder, Fields, ITask } from './src'

// On builder instanciation
let builder = new Builder({ todo: ['createdAt'], strict: true })
// builder.requiredTaskFields === []
// builder.requiredTodoFields === ['createdAt']

// after builder creation
builder = new Builder()
builder.setRequiredTodoFields(['title', 'description'])
builder.title('A Title').buildTodo() // fails to build for missing 'description' field

// Using some external value (maybe the back end tells you what fields are required). Type definition is required to pass it around safely
const taskFields: Fields<ITask> = ['title']
builder.setRequiredTaskFields(taskFields)
builder.setRequiredTodoFields([])
// builder.requiredTaskFields === ['title']
// builder.requiredTodoFields === []

Builder.setRequiredFields({ todo: ['createdAt'], task: ['description'] })
let builder2 = new Builder()
// builder2.requiredTaskFields === ['description']
// builder2.requiredTodoFields === ['createdAt']

```

for more in depth usage take a look at the tests and use your editor typings.

## License

released under MIT. do as you like.