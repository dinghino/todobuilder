/* istanbul ignore file */

import { ITodo, ITask } from '../src'
import * as moment from 'moment'
export const now: string = moment().toISOString()

/**
 * Mock data for external source
 */
export const mock: { todo: ITodo, task: ITask } = {
  todo: {
    title: 'A Todo',
    completed: false,
    createdAt: now,
    updatedAt: now,
  },
  task: {
    title: 'A Task',
    completed: false,
    createdAt: now,
    updatedAt: now,
    description: 'Task description',
    tags: ['general', 'testing'],
    dueTo: moment(now).add(2, 'hours').toISOString(),
    priority: 1,
  }
}

export default mock
