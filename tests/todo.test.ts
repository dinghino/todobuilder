/* istanbul ignore file */
import { Expect, Test, TestFixture, TestCase } from 'alsatian'
import Builder, { Todo, Task } from '../src'
import mock from './mock'

@TestFixture('Todo class')
export class TodoClassTests {
  @Test('Toggle Todo status')
  toggleTodo() {
    const todo = new Builder().title('A Title').buildTodo()
    Expect(todo.completed).toBe(false)
    todo.toggleCompleted()
    Expect(todo.completed).toBe(true)
  }

  @Test('Get Todo as JSON')
  getTodoJSON() {
    const todo = Builder.fromJSON(mock.todo).build()
    Expect(todo.toJSON()).toEqual({ type: 'todo', tags: [], ...mock.todo })
  }
  @Test('Get Task as JSON')
  getTaskJSON() {
    const task = Builder.fromJSON(mock.task).build()
    Expect(task.toJSON()).toEqual({ type: 'task', ...mock.task })
  }

  @Test('Discriminate Tasks')
  discriminateTasks() {
    const items = [Builder.fromJSON(mock.task).build(), Builder.fromJSON(mock.todo).build()]
    Expect(items[0].isTask()).toBe(true)
    Expect(items[1].isTask()).toBe(false)
    // Typescript compile test. Should compile with no errors.
    // This does actually nothing, just tests typescript capability of discriminating
    // the two classes.
    items.forEach(item => {
      if (item.isTask()) return item.dueTo
      else return item.updatedAt
    })
  }
  @Test('Correctly parse null moments')
  parsesNullMoments() {
    // setting just strict will remove all required field options
    const todo = new Builder({ strict: true }).buildTask().toJSON()
    Expect(todo.createdAt).toBeNull()
    Expect(todo.updatedAt).toBeNull()
    Expect(todo.dueTo).toBeNull()
  }
}
