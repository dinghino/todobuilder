/* istanbul ignore file */
import { Expect, Test, TestFixture, TestCase } from 'alsatian'
import Builder, { Todo, Task } from '../src'
import * as moment from 'moment'
import mock, { now } from './mock'


@TestFixture('TodoBuilder creation methods')
export class TodoBuilder {
  builder = new Builder()

  @Test('Reset after creation')
  resetAfterCreation() {
    const item = this.builder.title('A Title').build()
    // throws due title === null validation on build after reset
    Expect(item.title).toBe('A Title')
    Expect(this.builder.getState().data.title).toBeNull()
  }

  @Test('Sets default values')
  setDefaultValues() {
    const task = this.builder
      .title('A Title')
      .description('required')
      .dueTo(now)
      .buildTask()

    Expect(task.tags).toEqual([])
    Expect(task.completed).toBe(false)
    Expect(task.priority).toBe(0)
  }

  @Test('Creates all the properties')
  createsAllProperties() {
    const tags = ['test']
    const task = this.builder
      .title('Title')
      .description('Description')
      .createdAt(now)
      .updatedAt(now)
      .dueTo(now)
      .tags(tags)
      .priority(1)
      .buildTask()

    Expect(task.title).toBe('Title')
    Expect(task.description).toBe('Description')
    Expect(moment.isMoment(task.createdAt)).toBe(true)
    Expect(moment.isMoment(task.updatedAt)).toBe(true)
    Expect(moment.isMoment(task.dueTo)).toBe(true)
    Expect(task.tags).toBe(tags)
    Expect(task.priority).toBe(1)
  }

  @Test('Converts dates to moment')
  convertsDatesToMoment() {
    const todo = this.builder.title('A Title').updatedAt(now).createdAt(now).build()
    Expect(moment.isMoment(todo.updatedAt)).toBe(true)
    Expect(moment.isMoment(todo.createdAt)).toBe(true)
  }

  @Test('Create with moment date')
  createWithMomentDate() {
    const now = moment()
    const todo = this.builder.title('A Todo').createdAt(now).build()
    Expect(moment.isMoment(todo.createdAt)).toBe(true)
    Expect(todo.createdAt).toBe(now)
  }

  @Test('Alllows tags in various formats')
  variousTagsFormat() {
    let todo = this.builder.title('A Title').tags('a tag').build()
    Expect(todo.tags).toEqual(['a tag'])

    let todo2 = this.builder.title('A Title')
      .tags('tag1')
      .tags('tag2')
      .tags(['tag3', 'tag4'])
      .build()

      Expect(todo2.tags).toEqual(['tag1', 'tag2', 'tag3', 'tag4'])
  }

  @Test('Do not duplicates existing tags')
  doNotDuplicateTags() {
    let todo = this.builder.title('A Title').tags('tag').tags('tag').build()
    Expect(todo.tags).toEqual(['tag'])
    todo = this.builder.title('A Title').tags(['tag', 'tag']).tags('tag').build()
    Expect(todo.tags).toEqual(['tag'])
    todo = this.builder.title('A Title').tags(['tag', 'tag2']).tags('tag').build()
  }

  @Test('Handles various \'completed\' options')
  handleCompletedOpts() {
    // should set true by default
    let todo = this.builder.completed().title('A title').build()
    Expect(todo.completed).toBeTruthy()
    todo = this.builder.completed(false).title('A title').build()
    Expect(todo.completed).not.toBeTruthy()
  }

  @Test('Create from JSON')
  @TestCase('todo', 'Todo')
  @TestCase('task', 'Task')
  createFromJSON(data: keyof typeof mock, clsName: string) {
    const item = this.builder.fromJSON(mock[data]).build()
    Expect(item.constructor.name).toBe(clsName)
  }

  // Required fields testing --------------------------------------------------
  @Test('Uses custom required fields')
  customRequiredFields() {
    const builder = new Builder()
    const requiredTask = ['title', 'dueTo']
    const requiredTodo = ['title', 'createdAt']

    // passed directly due to array members mutability
    builder.setRequiredTaskFields(['title', 'dueTo'])
    builder.setRequiredTodoFields(['title', 'createdAt'])

    Expect(builder.requiredTaskFields).toEqual(requiredTask)
    Expect(builder.requiredTodoFields).toEqual(requiredTodo)

    // validate returns the missing require fields so we check on that too
    Expect(builder.validate('todo')).toEqual(requiredTodo)
    Expect(builder.validate('task')).toEqual(requiredTask)
  }

  @Test('No required fields builds and empty Todo object')
  noRequiredBuildsEmpty() {
    const builder = new Builder()
    // Test removing all required todo fields. It should build an empty object
    builder.setRequiredTodoFields([])
    let item: Todo
    Expect(() => item = builder.build()).not.toThrow()
    Expect(item).toBeDefined()
  }

  @Test('Options on creation')
  passOptionsOnCreation() {
    const builder = new Builder({
      todo: ['createdAt']
    })
    Expect(builder.requiredTodoFields).toEqual(['createdAt'])
    Expect(builder.requiredTaskFields).toEqual(Builder.REQUIRED_FIELDS.task)

    const strictBuilder = new Builder({
      todo: ['createdAt'],
      strict: true
    })
    Expect(strictBuilder.requiredTodoFields).toEqual(['createdAt'])
    Expect(strictBuilder.requiredTaskFields).toEqual([])
  }

  @Test('Set default required')
  setDefaultRequired() {
    let inst1 = new Builder()
    Expect(inst1.requiredTodoFields).toEqual([])
    Builder.setRequiredFields({ todo: ['title'] })
    Expect(Builder.REQUIRED_FIELDS.todo).toEqual(['_title'])
    let inst2 = new Builder()
    Expect(inst2.requiredTodoFields).toEqual(['title'])
    Builder.setRequiredFields()
    Expect(Builder.REQUIRED_FIELDS.todo).toEqual([])
  }

  // Validation tests ---------------------------------------------------------

  @Test('Returns missing fields on validate()')
  getMissingFields() {
    const builder = new Builder({ task: [ 'dueTo', 'title' ]}).title('A Title')
    Expect(builder.validate('task')).toEqual(['dueTo'])
  }
  @Test('Throws on missing title')
  throwsOnMissingTitle() {
    const builder = new Builder({ todo: ['title'] })
    Expect(() =>
      builder
      .completed(true)
      .build()
    ).toThrowError(Builder.MissingFieldError, 'Missing required field(s): title')
  }

  @Test('Throws on Task missing dueTo')
  throwsOnTaskMissingDueDate() {
    const builder = new Builder({ task: ['dueTo'] })
    Expect(() =>
      builder
      .title('A Task')
      .buildTask()
    ).toThrowError(Builder.MissingFieldError, 'Missing required field(s): dueTo')
  }

  @Test('Throws on invalid date format')
  throwsOnInvalidDate() {
    Expect(() => {
      this.builder.createdAt('not a date')
    }).toThrowError(TypeError, '\'not a date\' is not a valid date.')
  }

  @Test('Validator getters')
  validatorGetters() {
    let builder = new Builder({
      todo: ['title'],
      task: ['dueTo'],
    }).title('A Title')

    Expect(builder.isValidTask).toBe(false)
    Expect(builder.isValidTodo).toBe(true)
    Expect(builder.isValid).toBe(true)
    builder.dueTo('2018-01-01')
    Expect(builder.isValidTask).toBe(true)
    Expect(builder.isValid).toBe(true)
    builder.reset()
    Expect(builder.isValid).toBe(false)
  }
}
