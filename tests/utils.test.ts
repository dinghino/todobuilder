import { Expect, Test, TestFixture, TestCase } from 'alsatian'
import * as moment from 'moment'
import * as utils from '../src/utils'
import { truncate } from 'fs';

@TestFixture('Validator utilities')
export class ValidatorTests {
  @Test('isObject')
    @TestCase({}, true)
    @TestCase(null, false)
    @TestCase([], false)
    @TestCase('object', false)
    @TestCase(undefined, true, false)
    @TestCase(null, false, true)
  testIsObjectFunction(data: any, result: boolean, strict?: boolean) {
    Expect(utils.validate.isObject(data, strict)).toBe(result)
  }
  @Test('isString')
    @TestCase('a string', true)
    @TestCase('', true)
    @TestCase(123, false)
    @TestCase('', true, false)
    @TestCase(undefined, true, false)
  testIsString(data: any, result: boolean, strict?: boolean) {
    Expect(utils.validate.isString(data, strict)).toBe(result)
  }
  @Test('isNumber')
    @TestCase(1, true)
    @TestCase('1', false)
    @TestCase('1', true, false)
    @TestCase(1.23, true)
    @TestCase('abcd', false)
    @TestCase('', false, false)
    @TestCase(undefined, true, false)
  testIsNumber(data: any, result: boolean, strict?: boolean) {
    Expect(utils.validate.isNumber(data, strict)).toBe(result)
  }
  @Test('isInteger')
    @TestCase(1, true)
    @TestCase('1', false)
    @TestCase('1', true, false)
    @TestCase(1.23, false)
    @TestCase('1.23', false)
    @TestCase('1.23', false, false)
    @TestCase('', false)
    @TestCase('', false, false)
    @TestCase('not a number', false)
    @TestCase(undefined, false)
    @TestCase(undefined, true, false)
  testIsInteger(data: any, result: boolean, strict?: boolean) {
    Expect(utils.validate.isInteger(data, strict)).toBe(result)
  }
  @Test('isArray')
    @TestCase([], true)
    @TestCase(['item'], true)
    @TestCase(null, true, false)
    @TestCase(undefined, true, false)
    @TestCase(undefined, false)
    @TestCase('ab', false, false)
    @TestCase('', false)
    @TestCase('', false, false)
    @TestCase(0, false, false)
    @TestCase({}, false, false)
    @TestCase(123, false, false)
  testIsArray(data: any, result: boolean, strict?: boolean) {
    Expect(utils.validate.isArray(data, strict)).toBe(result)
  }
}

@TestFixture('Primitive transform utilities')
export class TransformerTest {
  @Test('toNumber handles string|number to number')
  @TestCase('1', 1)
  @TestCase(1, 1)
  handleStringToNumber(data: any,out: number) {
    Expect(utils.transform.toNumber(data)).toEqual(out)
  }
  @Test('toNumber throws on NaN values')
  @TestCase('not a number', null)
  NumberThrows(data: any) {
    Expect(() => utils.transform.toNumber(data)).toThrow()
  }
  @Test('toString converts to strings')
  @TestCase(1, '1')
  @TestCase('a string', 'a string')
  @TestCase({}, '[object Object]')  // FIXME: Should this fail?
  handleAnyToString(data: any,out: string) {
    Expect(utils.transform.toString(data)).toEqual(out)
  }
}

@TestFixture('Moment converter')
export class MomentConverter {
  @Test('Keep moments')
  keepMoments() {
    const now = moment()
    Expect(utils.transform.toMoment(now)).toBe(now)
  }
  @Test('Transform string dates')
  transformStrings() {
    const isoString = '2018-02-26T23:00:00.000Z'
    const normalFormat = '2018-02-27'

    const fromIso = utils.transform.toMoment(isoString)
    const fromNormal = utils.transform.toMoment(normalFormat)

    Expect(moment.isMoment(fromIso)).toBe(true)
    Expect(fromIso.isSame(isoString)).toBe(true)

    Expect(moment.isMoment(fromNormal)).toBe(true)
    Expect(fromNormal.isSame(normalFormat, 'day')).toBe(true)

  }
  @Test('Throws on not date values')
  throws() {
    Expect(() =>
      utils.transform.toMoment('not a date')
    ).toThrowError(TypeError, '\'not a date\' is not a valid date.')
  }

  @Test('Falsy values are unchanged')
  handleNull() {
    Expect(utils.transform.toMoment()).not.toBeDefined()
  }
}

