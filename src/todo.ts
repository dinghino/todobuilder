import * as moment from 'moment'

export type TODO_TYPE = 'task' | 'todo'

export interface ITodo {
  type?:        TODO_TYPE
  _id?:         string
  title:        string,
  completed:    boolean
  createdAt:    string
  updatedAt:    string
  tags?:        string[]
}

export interface ITask extends ITodo {
  description?: string
  dueTo?:       string,
  priority?:    number
}

export class Todo {
  readonly type: TODO_TYPE = 'todo'
  constructor(
    readonly title:      string,
    private _completed:  boolean,
    readonly tags:       string[],
    private _createdAt?: moment.Moment,
    private _updatedAt?: moment.Moment,
  ) {}

  get updatedAt(): moment.Moment { return this._updatedAt }
  get createdAt(): moment.Moment { return this._createdAt }
  get completed(): boolean { return this._completed }

  toggleCompleted(): void { this._completed = !this._completed }

  isTask(): this is Task { return false }

  toJSON(): ITodo {
    return {
      type:      this.type,
      title:     this.title,
      completed: this.completed,
      createdAt: moment.isMoment(this.createdAt) ? this.createdAt.toJSON() : this.createdAt,
      updatedAt: moment.isMoment(this.updatedAt) ? this.updatedAt.toJSON() : this.updatedAt,
      tags:      this.tags,
    }
  }
}

export class Task extends Todo {
  readonly type: TODO_TYPE = 'task'
  constructor(
    title:                string,
    readonly description: string,
    readonly dueTo:       moment.Moment,
    completed:            boolean,
    readonly priority:    number,
    createdAt?:           moment.Moment,
    updatedAt?:           moment.Moment,
    tags?:                string[],
  ) {
    super(title, completed, tags, createdAt, updatedAt)
  }

  // Tasks can be considered Todos too, so discriminating them is useless
  isTask(): this is Task { return true }

  toJSON(): ITask {
    const data = super.toJSON() as ITask
    
    data.dueTo =       moment.isMoment(this.dueTo) ? this.dueTo.toJSON() : this.dueTo
    data.description = this.description
    data.priority =    this.priority
    
    return data
  }
}
