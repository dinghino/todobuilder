import * as moment from 'moment'

import { TODO_TYPE, ITodo, ITask, Todo, Task } from './todo'

import { transform } from './utils'

export class MissingFieldError extends TypeError {
  constructor(fields: string[], message: string = 'Missing required field(s):') {
    const fieldString = fields.map(i => i.slice(1)).join(', ')
    message = `${message} ${fieldString}`
    super(message)
    Object.setPrototypeOf(this, MissingFieldError.prototype)
  }
}

export type Fields<T extends ITodo> = Array<keyof Partial<T>>

export interface RequiredFields {
  todo: Fields<ITodo>
  task: Fields<ITask>
}

export interface BuilderOptions {
  todo?: Fields<ITodo>
  task?: Fields<ITask>
  /**
   * if strict is true, only the passed required fields will be used,
   * even if a type is missing (resulting in no required fields) otherwise
   * what is missing will be taken from the default ones
   */
  strict?: boolean
}

export interface BuilderState {
  isValidTask: boolean
  isValidTodo: boolean
  required: {
    todo: Fields<ITask>
    task: Fields<ITask>
  }
  data: {
    type:        TODO_TYPE
    title:       string
    description: string
    tags:        string[]
    completed:   boolean
    priority:    number
    dueTo:       moment.Moment
    createdAt:   moment.Moment
    updatedAt:   moment.Moment
  }
}

export class Builder {
  private _type:        TODO_TYPE     = null
  private _title:       string        = null
  private _description: string        = null
  private _tags:        string[]      = null
  private _completed:   boolean       = null
  private _priority:    number        = null
  private _dueTo:       moment.Moment = null
  private _createdAt:   moment.Moment = null
  private _updatedAt:   moment.Moment = null

  /**
   * Custom TypeError that specifies what required fields are missing upon
   * building an entity
   */
  static MissingFieldError = MissingFieldError

  /** default required fields for all instances */
  static REQUIRED_FIELDS: RequiredFields = {
    todo: [],
    task: [],
  }

  /**
   * Change the default required fields for all new builder instances.
   * Does not affect the existing instances.
   */
  static setRequiredFields(fields: Partial<RequiredFields> = {}) {
    Builder.REQUIRED_FIELDS = {
      todo: Builder._normalizeReqFields(fields.todo),
      task: Builder._normalizeReqFields(fields.task),
    }
  }

  protected static _normalizeReqFields<T extends ITodo>(fields: Fields<T>): Fields<T> {
    return (fields || []).map(f => <keyof T>(f.startsWith('_') ? f : `_${f}`))
  }

  /**
   * Describes what fields are required for todos and tasks objects.
   */
  private REQUIRED_FIELDS: RequiredFields = {
    todo: [],
    task: [],
  }

  constructor(private options: BuilderOptions = {}) {
    this.setup(options)
  }

  private setup(options: BuilderOptions) {
    const todoFields = options.todo || (options.strict ? [] : Builder.REQUIRED_FIELDS.todo)
    const taskFields = options.task || (options.strict ? [] : Builder.REQUIRED_FIELDS.task)

    this.REQUIRED_FIELDS = {
      todo: Builder._normalizeReqFields(todoFields),
      task: Builder._normalizeReqFields(taskFields),
    }
  }

  setRequiredTodoFields(fields: Fields<ITodo>): this {
    this.REQUIRED_FIELDS.todo = Builder._normalizeReqFields(fields)
    return this
  }
  setRequiredTaskFields(fields: Fields<ITask>): this {
    this.REQUIRED_FIELDS.task = Builder._normalizeReqFields(fields)
    return this
  }

  get requiredTodoFields(): Fields<ITask> {
    return this.REQUIRED_FIELDS.todo.map(f => <keyof ITodo>f.slice(1))
  }
  get requiredTaskFields(): Fields<ITask> {
    return this.REQUIRED_FIELDS.task.map(f => <keyof ITask>f.slice(1))
  }

  // Setters =================================================================

  title(value: string): this {
    this._title = value
    return this
  }
  description(value: string): this {
    this._description = value
    return this
  }
  completed(value: boolean = true): this {
    // set up when we do input validation
    // if (value && typeof value !== 'boolean') throw new TypeError('Completed requires a \'boolean\' value.')
    this._completed = !!value
    return this
  }
  priority(value: number): this {
    // set up when we do input validation
    // if (isNaN(value)) throw new TypeError('Priority requires a \'number\' value.')
    this._priority = value
    return this
  }

  tags(tags: string | string[]): this {
    if (!tags) return this

    const { _tags } = this
    if (!Array.isArray(tags)) tags = [tags]

    if (!_tags || !_tags.length) this._tags = tags
    else this._tags = this._tags
      .concat(tags)
      // remove duplicates
      .filter((el, pos, self) => self.indexOf(el) === pos)

    return this
  }

  dueTo(date: string | moment.Moment): this {
    this._dueTo = transform.toMoment(date)
    return this
  }
  createdAt(date: string | moment.Moment): this {
    this._createdAt = transform.toMoment(date)
    return this
  }
  updatedAt(date: string | moment.Moment): this {
    this._updatedAt = transform.toMoment(date)
    return this
  }

  // Factory methods ==========================================================

  buildTodo(): Todo {
    this._validate(this.REQUIRED_FIELDS.todo, true)
    const item = new Todo(
      this._title,
      this._completed || false,
      this._tags || [],
      this._createdAt,
      this._updatedAt,
    )
    this.reset()
    return item
  }
  buildTask(): Task {
    this._validate(this.REQUIRED_FIELDS.task, true)
    const item = new Task(
      this._title,
      this._description,
      this._dueTo,
      this._completed || false,
      this._priority || 0,
      this._createdAt,
      this._updatedAt,
      this._tags || [],
    )
    this.reset()
    return item
  }

  build(): Todo
  build(): Task
  build() {
    return this.isTask ? this.buildTask() : this.buildTodo()
  }

  reset(): void {
    this._type        = null
    this._title       = null
    this._completed   = null
    this._createdAt   = null
    this._updatedAt   = null
    this._tags        = null
    this._description = null
    this._dueTo       = null
    this._priority    = null
  }

  fromJSON(d: ITask): this
  fromJSON(d: ITodo): this
  fromJSON(data: ITodo | ITask) {
    const d = data as ITask
    // This is fail prone but is quite shorter, dynamic and allows for
    // adding methods and schemas at runtime. requires validation on methods
    // Object.keys(data).forEach(key =>
    //   !!this[key] && typeof this[key] === 'function' && this[key](d[key])
    // )

    return this
      .title(d.title)
      .completed(d.completed)
      .updatedAt(d.updatedAt)
      .createdAt(d.createdAt)
      .description(d.description)
      .dueTo(d.dueTo)
      .priority(d.priority)
      .tags(d.tags)
  }

  static fromJSON(d: ITodo, options?: BuilderOptions): Builder
  static fromJSON(d: ITask, options?: BuilderOptions): Builder
  static fromJSON(d: ITask | ITodo, options?: BuilderOptions) {
    return new Builder(options).fromJSON(d)
  }

  // Validators ==============================================================

  get isTask(): boolean {
    return (
      !!this._dueTo         // dueTo is the only required extra property for Tasks
      // !!this._description   // description is unique to tasks but not required.
    )
  }

  /**
   * internal validation method that loops an array of strings and checks that
   * they exist on the builder object.
   * If one or more are missing resets the builder and throws a MissingError
   */
  private _validate(fields: string[], throws?: boolean): string[] {
    let errors: string[] = []
    fields.forEach(field => {
      if (!(<any>this)[field]) errors.push(field)
    })

    if (errors.length && throws)
      throw new MissingFieldError(errors)

    return errors
  }

  validate(asType: 'todo' | 'task') {
    const fields = asType === 'todo' ? this.REQUIRED_FIELDS.todo : this.REQUIRED_FIELDS.task
    return this._validate(fields).map(field => field.slice(1))
  }

  /** True if the builder can create a valid Todo object */
  get isValidTodo(): boolean {
    return this.REQUIRED_FIELDS.todo.reduce((p, n)=> !(<any>this)[n] ? false : p, true)
  }
  /** True is the builder can create a valid Task object */
  get isValidTask(): boolean {
    return this.REQUIRED_FIELDS.task.reduce((p, n)=> !(<any>this)[n] ? false : p, true)
  }
  /** True if the builder can create a valid object regardless of the type */
  get isValid(): boolean {
    return this.isValidTodo || this.isValidTask
  }

  /**
   * return the internal state of the builder, with all its properties and
   * validation
   */
  getState(): BuilderState {
    return {
      isValidTodo: this.isValidTodo,
      isValidTask: this.isValidTask,
      required: {
        todo: this.requiredTodoFields,
        task: this.requiredTaskFields,
      },
      data: {
        type:        this._type,
        title:       this._title,
        description: this._description,
        tags:        this._tags,
        completed:   this._completed,
        priority:    this._priority,
        dueTo:       this._dueTo,
        createdAt:   this._createdAt,
        updatedAt:   this._updatedAt,
      }
    }
  }

}

export default Builder
