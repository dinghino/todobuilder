
import * as moment from 'moment'

const isNull = (v: any) => v === undefined || v === null

/**
 * Simple validation functions for type checking on runtime.
 * Each function requires a `val`ue data and an optional strict mode that, if
 * set to true, will allow null values and return true, to quickly validate stuff
 * that we don't care about if they are there or not.
 * When strict mode is off the validation will work as intended (and will return false
 * on most cases with falsey values.)
 */
export namespace validate {

  export function isObject(val: any, strict = true): boolean {
    if (!strict && isNull(val)) return true
    return val === Object(val) && !Array.isArray(val)
  }
  export function isString(val: string, strict = true): boolean {
    if (!strict && isNull(val)) return true
    return typeof val === 'string'
  }
  /**
   * Whether a value is a number or not.
   * Strict mode off will allow string-represented numbers (i.e. '1') to be true
   */
  export function isNumber(val: any, strict = true): boolean {
    const nullValue = isNull(val)
    if (strict) {
      if (nullValue || typeof val !== 'number') return false
      return true
    }
    // non-strict mode allows null value and everything that can be converted
    // to number is valid. We use parseFloat for special cases like '' that would
    // return 0 when converted to numbers with the + operator or other means.
    if (nullValue) return true
    if (isNaN(parseFloat(val))) return false
    return true
  }
  export function isInteger(val: any, strict = true): boolean {
    if (!strict)
      if (isNull(val)) return true

    if (!isNumber(val, strict)) return false
    // If the converted float is a x.0 we consider it to be an integer
    return Number.isInteger(parseFloat(val))
  }
  export function isArray(val: any, strict = true): boolean {
    if (!strict)
      if (isNull(val)) return true

    return Array.isArray(val)
  }

}
/**
 * Contains a bunch of simple transformation functions that ensure that a value
 * is actually what we need.
 * @throws each function throws type error if the conversion is not possible.
 */
export namespace transform {

  export function toMoment(val?: string | moment.Moment): moment.Moment {
    if (isNull(val)) return
    if (moment.isMoment(val)) return val
    /**
     * check that the src argument is actually a date. This hack is due to
     * moment throwing a deprecation warning when trying to build a Moment
     * from an invalid format, while Date.parse returns NaN silently.
     */
    if (isNaN(Date.parse(val)))
      throw new TypeError(`'${val}' is not a valid date.`)

    return moment(val)
  }

  export function toNumber(val: string | number) {
    const v = +val
    if (isNaN(v))
      throw new TypeError(`${val} should be a number or a number-like object. It's a ${typeof val} instead.`)
    return v
  }

  export function toString(val: string | number) {
    if (typeof val === 'string') return val
    // throws automatically if method is missing so no need to do it
    return val.toString()
  }

}
